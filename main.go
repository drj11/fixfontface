package main

// fixfontface

// In an HTML file, fix font CSS properties to refer to a freshly created
// @font-face declaration.

import (
	"flag"
	"fmt"
	"golang.org/x/net/html"
	"log"
	"os"
	"regexp"
	"unicode"
	"unicode/utf8"
)

var matchFlag = flag.String("match", "(.*)", "Regular expression to match")

func main() {

	flag.Parse()

	matcher, err := regexp.Compile(*matchFlag)
	if err != nil {
		log.Fatal(err)
	}
	if matcher.NumSubexp() < 1 {
		log.Fatal("match RE should contain at least one group")
	}

	filename := flag.Arg(1)
	r, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	doc, err := html.Parse(r)
	if err != nil {
		log.Fatal(err)
	}

	fix(doc, matcher, flag.Arg(0))

	html.Render(os.Stdout, doc)
}

// edit doc so that the CSS font styles are changed.
func fix(doc *html.Node, matcher *regexp.Regexp, fontURL string) {
	// from https://pkg.go.dev/golang.org/x/net/html#example-Parse
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "style" {
			isCSS := false
			for _, a := range n.Attr {
				if a.Key != "type" || a.Val != "text/css" {
					continue
				}
				isCSS = true
			}
			if isCSS {
				cssText := glomchild(n)
				newCSS := edit(cssText,
					matcher,
					fontURL)

				for n.FirstChild != nil {
					n.RemoveChild(n.FirstChild)
				}
				n.AppendChild(&html.Node{Type: html.TextNode,
					Data: newCSS})
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
}

func edit(css string, matcher *regexp.Regexp, fontURL string) string {
	famre := regexp.MustCompile("font:.*(%|em|ex|in|cm|mm|pt|pc|px) +(.*) *;")

	fs := map[string]struct{}{}

	edited := ""
	tail := css
	for {
		m := famre.FindStringSubmatchIndex(tail)
		if m == nil {
			break
		}

		fami := m[4]
		famj := m[5]

		edited += tail[:fami]
		family := tail[fami:famj]
		tail = tail[famj:]

		families := truefamily(family)
		{
			// Only test the first family name in a sequence.
			target := families[0]
			m := matcher.FindStringSubmatch(target)
			if m != nil {
				// edit the family
				for i := 1; i < len(m); i += 1 {
					if m[i] != "" {
						family = m[i]
						break
					}

				}
			}
		}

		fs[family] = struct{}{}
		family = familyescape(family)
		edited += family
	}

	css = edited + tail

	for f, _ := range fs {
		atrule := fmt.Sprintf("@font-face {\n"+
			"font-family: %s;\n"+
			"src: url(\"%s\");\n"+
			"}\n",
			familyescape(f),
			fontURL,
		)
		css = atrule + css
	}
	return css
}

// glom (join) all the child nodes into single string.
func glomchild(n *html.Node) string {
	s := ""
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		s += c.Data
	}
	return s
}

// This RE is to match identifiers, but
// the full syntax seems a bit more horrible.
var CSSIdentRe = regexp.MustCompile("^[A-Za-z_][-0-9A-Za-z_]*$")

// Escape the familyname so that it conforms to CSS
func familyescape(family string) string {
	if !CSSIdentRe.MatchString(family) {
		log.Fatalf("Font family name %q cannot be escaped (not implemented).", family)
	}
	// :todo: implement me
	return family
}

// convert a font-family property string into canonical form.
// https://www.w3.org/TR/CSS21/fonts.html#propdef-font-family
// From the CSS spec, the grammar rule is:
// [ <family-name>|<generic-family> ] [, <family-name>|<generic-family> ]*
// family-name appears not to have a grammar production, but
// the supporting text says
// "Font family names must either be given quoted as strings,
// or unquoted as a sequence of one or more identifiers."
// The comma-separated list is converted to a slice of strings,
// one per family name.
// Each family name is converted to its string value;
// where the family name is a sequence of identifiers (not a
// string), the identifiers are joined with a single space (the
// same algorithm that CSS uses to compare font family names).
func truefamily(family string) []string {
	res := []string{}

	// :todo: This should really be a (very simple) parser.
	_, items := lex("CSS family name", family)
	for item := range items {
		res = append(res, item.val)
	}
	return res
}

// ## Lexer
// https://talks.golang.org/2011/lex.slide#22

type itemType int
type item struct {
	typ itemType
	val string
}

const (
	itemError itemType = iota
	itemEOF
	itemComma
	itemString
	itemIdentifier
)

// lexer holds the state of the scanner.
type lexer struct {
	name  string    // used only for error reports.
	input string    // the string being scanned.
	start int       // start position of this item.
	pos   int       // current position in the input.
	width int       // width of last rune read from input.
	items chan item // channel of scanned items.
}

// stateFn represents the state of the scanner
// as a function that returns the next state.
type stateFn func(*lexer) stateFn

func lex(name, input string) (*lexer, chan item) {
	l := &lexer{
		name:  name,
		input: input,
		items: make(chan item),
	}
	go l.run() // Concurrently run state machine.
	return l, l.items
}

// run lexes the input by executing state functions until
// the state is nil.
func (l *lexer) run() {
	for state := lexInitial; state != nil; {
		state = state(l)
	}
	close(l.items) // No more tokens will be delivered.
}

// emit passes an item back to the client.
func (l *lexer) emit(t itemType) {
	l.items <- item{t, l.input[l.start:l.pos]}
	l.start = l.pos
}

func lexInitial(l *lexer) stateFn {
	r, size := utf8.DecodeRuneInString(l.input[l.pos:])
	if size == 0 {
		return nil
	}
	if r == utf8.RuneError {
		log.Fatal("lex error; invalid utf-8", []byte(l.input[l.pos:]))
	}
	if unicode.IsSpace(r) {
		l.pos += size
		l.start = l.pos
		return lexInitial
	}

	if r == '\'' {
		return lexStringSingleQ
	}
	if r == '"' {
		return lexStringDoubleQ
	}
	return lexIdent
}

func lexStringSingleQ(l *lexer) stateFn {
	v := ""
	l.pos += 1
	l.start = l.pos
	for {
		if l.input[l.pos] == '\'' {
			l.items <- item{itemString, v}
			l.pos += 1
			l.start = l.pos
			return lexInitial
		}
		if l.input[l.pos] == '\\' {
			// backslash newline continues a long string:
			// ignore those characters, but keep lexing.
			if l.input[l.pos+1] == '\n' {
				l.pos += 2
				continue
			}

			// https://www.w3.org/TR/css-syntax-3/#consume-an-escaped-code-point
			// consume 1 to 6 hex digits
			h := "" // hex characters in hex escape
			l.pos += 1
			for {
				r, _ := utf8.DecodeRuneInString(l.input[l.pos:])
				if unicode.In(r, unicode.ASCII_Hex_Digit) {
					h += l.input[l.pos : l.pos+1]
					l.pos += 1
					if len(h) == 6 {
						break
					}
				} else {
					break
				}
			}
			if len(h) == 0 {
				l.items <- item{itemError, "bad escape in CSS string, see https://www.w3.org/TR/css-syntax-3/#consume-a-string-token"}
			}
			var x rune
			fmt.Sscanf(h, "%x", &x)
			v += string(x)

			// consume an immediately following whitespace, if present
			r, size := utf8.DecodeRuneInString(l.input[l.pos:])
			if size == 0 {
				return nil
			}
			if r == utf8.RuneError {
				log.Fatal("lex error; invalid utf-8", []byte(l.input[l.pos:]))
			}
			if unicode.IsSpace(r) {
				l.pos += size
				l.start = l.pos
			}
		}

		// normal character, add to value
		v += l.input[l.pos : l.pos+1]
		l.pos += 1
		l.start = l.pos
	}
}

func lexStringDoubleQ(l *lexer) stateFn {
	v := ""
	l.pos += 1
	l.start = l.pos
	for {
		if l.input[l.pos] == '"' {
			l.items <- item{itemString, v}
			l.pos += 1
			l.start = l.pos
			return lexInitial
		}
		if l.input[l.pos] == '\\' {
			// backslash newline continues a long string:
			// ignore those characters, but keep lexing.
			if l.input[l.pos+1] == '\n' {
				l.pos += 2
				continue
			}

			// https://www.w3.org/TR/css-syntax-3/#consume-an-escaped-code-point
			// consume 1 to 6 hex digits
			h := "" // hex characters in hex escape
			l.pos += 1
			for {
				r, _ := utf8.DecodeRuneInString(l.input[l.pos:])
				if unicode.In(r, unicode.ASCII_Hex_Digit) {
					h += l.input[l.pos : l.pos+1]
					l.pos += 1
					if len(h) == 6 {
						break
					}
				} else {
					break
				}
			}
			if len(h) == 0 {
				l.items <- item{itemError, "bad escape in CSS string, see https://www.w3.org/TR/css-syntax-3/#consume-a-string-token"}
			}
			var x rune
			fmt.Sscanf(h, "%x", &x)
			v += string(x)

			// consume an immediately following whitespace, if present
			r, size := utf8.DecodeRuneInString(l.input[l.pos:])
			if size == 0 {
				return nil
			}
			if r == utf8.RuneError {
				log.Fatal("lex error; invalid utf-8", []byte(l.input[l.pos:]))
			}
			if unicode.IsSpace(r) {
				l.pos += size
				l.start = l.pos
			}
		}

		// normal character, add to value
		v += l.input[l.pos : l.pos+1]
		l.pos += 1
		l.start = l.pos
	}
}

func lexIdent(l *lexer) stateFn {
	l.items <- item{itemError, "IDENT not implemented"}
	return nil
}
