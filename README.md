# fixfontface

Usage:

    fixfontface [-match RE] font.otf page.html

The HTML file is scanned for references to fonts in the style
and replaces those references to a particular font-file.

New @font-face CSS at-rules are created that map the font-family
names in style rules to the font-file.

If an RE is supplied to the `-match` option then only
font-family names that match the RE are mapped to the font-file
and the font-family name is changed to be the first non-empty
match of the RE.
The default for this option is `(.*)` which matches everything
and leaves the font-family name unchanged.

The intended purpose of the `-match` option is in the font
development process used by Cubic Type.
Typically there will be _many_ development fonts of the form:
«Family LAB CCYYMMDD».
Using a RE like `(.*) +LAB.*` means that the font-family names
will get rewritten to use the root name that does not have the
«LAB 20220216» part.

You may wish to match using a regular expression that strips
`LAB.*` only if present.
This is a little bit tricky, but a second alternate works:

    fixfontface -match '(.*\w) *LAB.*|(.*)' font.otf Specimen.html

If you use the `-match` option to remove optional suffixes, a
final `|(.*)` alternate is probably a good idea.


## BUGS

The @font-face rules are not actually added.

CSS is scanned using regular expressions, so it will probably
all go wrong.

The Font Family Name, possibly transformed by the `-match`
regular expression, is supposed to be escaped before embedding
back in the CSS.
This only works for identifiers that do not need escaping.
In particular,
any multi-word name will fail (for example «Twentieth Century»).

# END
